echo "Commit: $GIT_COMMIT"
GIT_COMMIT_NUMBER=$(echo $GIT_COMMIT | head -c 7)
ARR=$(git diff-tree --no-commit-id --name-only --diff-filter=A --diff-filter=M -r $GIT_COMMIT_NUMBER)
CHANGED_FILES=$(echo $ARR)
if [ -z "$CHANGED_FILES" ]
then
      echo "No PHP changed file"
else
      $1/vendor/bin/phpcs --standard=Magento2 $CHANGED_FILES --extensions=php
fi