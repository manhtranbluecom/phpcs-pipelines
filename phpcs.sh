git fetch origin "+refs/heads/*:refs/remotes/origin/*" -q

#git --no-pager diff origin/develop --name-only --diff-filter=A --diff-filter=M

git diff --name-only --diff-filter=AM origin/$BITBUCKET_PR_DESTINATION_BRANCH... -- '*.php'

ARR=$(git diff --name-only --diff-filter=AM origin/$BITBUCKET_PR_DESTINATION_BRANCH... -- '*.php')
CHANGED_FILES=$(echo $ARR)

#Get 7 first characters from commit hash
#BITBUCKET_COMMIT_NUMBER=$(echo $BITBUCKET_COMMIT | head -c 7)

#echo $BITBUCKET_COMMIT_NUMBER;

#git diff-tree --no-commit-id --name-only --diff-filter=A --diff-filter=M -r $BITBUCKET_COMMIT_NUMBER

#ARR=$(git diff-tree --no-commit-id --name-only --diff-filter=A --diff-filter=M -r $BITBUCKET_COMMIT_NUMBER)

if [ -z "$CHANGED_FILES" ]
then
      echo "No PHP changed file"
else
      magento-coding-standard-develop/vendor/bin/phpcs --standard=Magento2 $CHANGED_FILES --extensions=php
fi
