git fetch origin "+refs/heads/*:refs/remotes/origin/*" -q

git diff --name-only --diff-filter=AM origin/$BITBUCKET_PR_DESTINATION_BRANCH... -- '*.js'

ARR=$(git diff --name-only --diff-filter=AM origin/$BITBUCKET_PR_DESTINATION_BRANCH... -- '*.js')
CHANGED_FILES=$(echo $ARR)

if [ -z "$CHANGED_FILES" ]
then
      echo "No Js changed file"
else
      curl https://raw.githubusercontent.com/magento/magento2/2.4/dev/tests/static/testsuite/Magento/Test/Js/_files/eslint/.eslintrc-magento --output eslint.json
      npx eslint --no-eslintrc -c eslint.json $CHANGED_FILES
fi
