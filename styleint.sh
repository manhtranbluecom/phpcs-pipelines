git fetch origin "+refs/heads/*:refs/remotes/origin/*" -q

git diff --name-only --diff-filter=AM origin/$BITBUCKET_PR_DESTINATION_BRANCH... -- '*.less' '*.css'

ARR=$(git diff --name-only --diff-filter=AM origin/$BITBUCKET_PR_DESTINATION_BRANCH... -- '*.less' '*.css')
CHANGED_FILES=$(echo $ARR)

if [ -z "$CHANGED_FILES" ]
then
      echo "No style file changed"
else
      curl -OL https://bitbucket.org/manhtranbluecom/phpcs-pipelines/raw/da751a24172a94c891b89867672399627b06a2c0/.stylelintrc.json
      npx stylelint $CHANGED_FILES --config .stylelintrc.json
fi
